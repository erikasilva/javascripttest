# README #
# Javascript Test - The Ricky and Morty API
Desarrollo de un website usando Javascript y el API de The Ricky and Morty.

## Tabla de Contenidos

- [Introduccion](#introduccion)
- [Descripcion de la Solucion](#descripcion-de-la-solucion)
- [Clonacion](#clonacion)
- [Ejecucion](#ejecucion)
- [Ejemplo de ejecucion](#ejemplo-de-ejecucion)


## Introduccion
>Descripción del ejercicio

- Usar api de rick and morty <https://rickandmortyapi.com> para maquetar un website con Vue3.js.
- Crear una pantalla con un header/menu que permita al usuario redirigirse entre dos secciones [personajes, episodios].
- Dentro de personajes consumir el siguiente endpoint <https://rickandmortyapi.com/api/character/> para renderizar cada personaje en una grid de 4 columnas. Cada personaje debe ser representado dentro de una tarjeta que contenga los siguientes datos: name, status e image
- Dentro de episodios consumir el siguiente endpoint <https://rickandmortyapi.com/api/episode> para renderizar cada episodio dentro de una columna en forma de lista. Cada episodio debe ser representado con los siguientes datos: name, air_date y characters.

Ambas secciones:

- Implementar paginación que se activara por acción de un scroll.
- Cada que ingrese a la sección debe actualizarse a la primera pagina del endpoint.
- El primer render debe tener auto seleccionado la sección episodios


## Descripcion de la Solucion
- La solucion del ejercicio se realizó en Javascript, HTML y CSS en el IDE de desarrollo Visual Studio Code.
- Se utilizó la función `fetch()` para recuperar los datos del API en un JSON.
- Se mostró los datos en pantalla mediante el uso de estilos CSS.
- Se realizó la paginación activada por el scroll con la funcion `window.onscroll()`

## Clonacion

- Para clonar este repositorio localmente usar: `git clone https://erikasilva@bitbucket.org/erikasilva/javascripttest.git`


## Ejecucion
Para poder mostrar en el navegador la solución se utilizó Node JS y Express JS.

El ejercicio se puede ejecutar con la siguiente expresión:
`node index.js`

## Ejemplo de ejecucion

#### Sección de Episodios

![alt text](https://bitbucket.org/erikasilva/javascripttest/raw/2c660dc4b9f9476c7941259bbd0686fc9089a455/public/images/episodes.png "")

#### Sección de Personajes

![alt text](https://bitbucket.org/erikasilva/javascripttest/raw/2c660dc4b9f9476c7941259bbd0686fc9089a455/public/images/characters.png "")

## Autor
- Erika Silva.
