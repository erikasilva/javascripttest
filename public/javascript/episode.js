fetch('https://rickandmortyapi.com/api/episode')
    .then(response => response.json())
    .then(json => createEpisodesList(json))

window.onscroll = async function(ev) {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {

        var nextPage = document.getElementById("episodesList").getAttribute("next");
        if (nextPage != "null") {
            await fetch(nextPage)
                .then(response => response.json())
                .then(json => createEpisodesList(json))
        }
    }
    setTimeout(function() {}, 500);
}

function createEpisodesList(episodesJSON) {
    document.getElementById("episodesList").setAttribute("next", episodesJSON.info.next);
    const episodesList = document.querySelector('#episodesList');
    episodesJSON.results.forEach(element => {
        createCharactersList(element.characters, element.id);
        episodesList.innerHTML = episodesList.innerHTML + `<li id="charactersList${element.id}">
        <h3>Name: ${element.name}</h3>
        <p><strong>Air-date:</strong> ${element.air_date}</p>
        <h3>Characters:</h3>
        </li>`;
    });
}

function createCharactersList(charactersArray, id) {
    const charactersNameList = document.createElement('ul');
    Promise.all(charactersArray.map(element => {
        fetch(element, { mode: 'cors' })
            .then(response => response.json())
            .then(json => {
                charactersNameList.innerHTML = charactersNameList.innerHTML + `<li>
                <p>${json.name}</p>
                </li>`
                document.getElementById("charactersList" + id).appendChild(charactersNameList);
            })
    }))
}