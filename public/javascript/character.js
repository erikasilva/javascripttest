fetch('https://rickandmortyapi.com/api/character')
    .then(response => response.json())
    .then(json => createCharactersCards(json))

window.onscroll = async function(ev) {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        await fetch(document.getElementById("cardsDiv").getAttribute("next"))
            .then(response => response.json())
            .then(json => createCharactersCards(json))
    }
    setTimeout(function() {}, 500);
}

function createCharactersCards(charactersJSON) {
    document.getElementById("cardsDiv").setAttribute("next", charactersJSON.info.next);
    const cardsDiv = document.querySelector('#cardsDiv');
    charactersJSON.results.forEach(element => {
        cardsDiv.innerHTML = cardsDiv.innerHTML + `<div class="card">
        <img src="${element.image}" style="width:100%">
        <div class="container">
        <h2>${element.name}</h2>
        <p><strong>Status:</strong> ${element.status}</p></div></div>`
    });
}